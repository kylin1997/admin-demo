import Mock from "mockjs";

Mock.mock('/demo/query', req => {

  let queryForm = JSON.parse(req.body)

  console.log(queryForm);

  let pageSize = queryForm.pageSize

  let Random = Mock.Random;
  let rowCount = Random.integer(60, 100);

  let data = [];
  for (let i = 0; i < pageSize; i++) {
    data.push({
      id: Random.id(),
      name: Random.cname(),
      age: Random.integer(18, 19),
      interest: Random.integer(1, 2),
      nativePlace: Random.integer(1, 3),
      birthday: Random.date('yyyy-MM-dd'),
      desc: Random.csentence()
    });
  }

  return {
    success: true,
    code: 200,
    message: 'success',
    data: {
      rowCount:rowCount,
      data:data
    }
  };
});

Mock.mock('/demo/add', req => {
  let row = JSON.parse(req.body)
  row.id = Mock.Random.id()

  return {
    success: true,
    code: 200,
    message: 'success',
    data: row
  };

});

Mock.mock('/demo/update', req => {
  let row = JSON.parse(req.body)

  console.log('update:' + JSON.stringify(row))

  return {
    success: true,
    code: 200,
    message: 'success',
    data: row
  }
});

Mock.mock('/demo/remove', req => {
  let result = Mock.Random.boolean()
  if (result) {
    return {
      success: true,
      code: 200,
      message: 'success'
    }
  } else {
    return {
      success: false,
      code: Mock.Random.integer(300, 599),
      message: '测试,删除失败'
    }
  }
});


Mock.mock('/demo/approve', req => {
  let row = JSON.parse(req.body)

  console.log(row)

  return {
    success: true,
    code: 200,
    message: 'success'
  }
});

Mock.mock('/demo/xxxx', req => {

});
