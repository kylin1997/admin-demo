export const editable = (field) => {
  if(field && (field.editable == false || !field.label || !field.key))
    return false;
  else
    return true;
}
