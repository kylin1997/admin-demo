import {dateUtils} from './admin-utils'

const needRender = (field) => {

  if (isValueEnum(field) || isDateFormat(field)) {
    return true
  } else {
    return false
  }

}

const renderColumn = (field, row, column, h) => {
  if(isValueEnum(field)){
    return renderValueEnum(field,row,column,h)
  } else if(isDateFormat(field)){
    return renderDateFormat(field,row,column,h)
  } else {
    return h('div', ['无法识别渲染函数'])
  }
}
const renderDateFormat = (field, row, column, h) => {
  let columnValue = row[column.key]
  let dateFormat = field.dateFormat

  let text = ''

  if(columnValue && columnValue instanceof Date)
    text = dateUtils.format(columnValue,dateFormat)
  else
    text = columnValue

  return h('div', [text])
}
const renderValueEnum = (field, row, column, h) => {
  let columnValue = row[column.key]

  let labelArray = [];

  if (columnValue instanceof Array) {
    columnValue.forEach((value,index) => {
      let label = getLabel(field, value)
      if (label != undefined) {
        if(index > 0)
          labelArray.push(',')
        labelArray.push(label)
      }
    });
  } else {
    let label = getLabel(field, columnValue)
    if (label != undefined)
      labelArray.push(label)
  }

  return h('div', labelArray)
}

const isValueEnum = field => {
  return field.valueEnum instanceof Array && field.valueEnum.length > 0
}
const isDateFormat = field => {
  return field.dateFormat != undefined && (field.dateFormat instanceof String) && field.dateFormat.length > 0
}

const getLabel = (field, columnValue) => {
  let valueEnum = field.valueEnum
  let valueRow = valueEnum.find(row => row.value === columnValue);
  if (valueRow != undefined)
    return valueRow.label
  else
    return undefined
}
// const isRadio

export default {
  needRender,
  renderColumn
}
