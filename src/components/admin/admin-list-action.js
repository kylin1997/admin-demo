import axios from '@/libs/api.request'

export const displayable = (field) => {
  if(field && (field.displayable == false || !field.label || !field.key))
    return false;
  else
    return true;
}

const loadData = (url, queryData) => {
  return axios.request({
    url: url,
    method: 'post',
    data:queryData
  }).then(handleResponse)
}

const loadDataCount = (url,queryData) =>{
  return axios.request({
    url: url,
    method: 'post',
    data:queryData
  }).then(handleResponse)
}

const addRow = (url,requestData) =>{
  return axios.request({
    url: url,
    method: 'post',
    data:requestData
  }).then(handleResponse)
}

const editRow = (url,requestData) =>{
  console.log("submit:")
  console.log(requestData)
  return axios.request({
    url: url,
    method: 'post',
    data:requestData
  }).then(handleResponse)
}

const removeRow = (url,requestData) =>{
  return axios.request({
    url: url,
    method: 'post',
    data:requestData
  }).then(handleResponse)
}

const submit = (url,row) => {
  return axios.request({
    url: url,
    method: 'post',
    data:row
  }).then(handleResponse)
}

const handleResponse = response => {

  let result = response.data

  if(result.success === true)
    return result.data
  else
    return new Error(result.message)
}

const showButton = (action,row) => {
  let fun = action.showConditionFun
  if(fun && fun instanceof Function){
    return fun(row);
  } else
    return true;//如果没有设置showConditionFun属性，或者设置的不正确，直接显示按钮，默认显示按钮。
}


export const request = {
  loadData,
  loadDataCount,
  addRow,
  editRow,
  removeRow,
  submit
}

export const actionUtils = {
  showButton
}
