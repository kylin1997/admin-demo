import axios from '@/libs/api.request'

export default {
  namespaced: true,
  state: {
    demoList: []
  }, // 状态
  mutations: {
    SET_CORE_KPI: (state, kpiList) => {
      console.log(kpiList)

      state.demoList = []

      kpiList.forEach((kpi) => state.demoList.push(kpi))
    }

  }, // 状态更新事件_同步_事件
  actions: {
    queryCoreKpi: (context, formItem) => {
      let args = {
        age: formItem.input
      }

      return axios.request({
        url: '/api/get_core_kpi',
        method: 'post',
        data: args
      }).then((resp) => {
        context.commit('SET_CORE_KPI', resp.data)
      })
    }
  }, // 异步执行函数
  getters: {
    coreKpiA: state => {
      return state.coreKpi * 100
    }
  }// 计算属性
}
